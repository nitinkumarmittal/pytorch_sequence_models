import io
from collections import Counter
from typing import List

import torch
from spacy import tokenizer
from torchtext.vocab import Vocab


def tokenize(text: str, tokenizer: tokenizer.Tokenizer) -> List[str]:
    """Return list of str tokens."""
    return [token.text for token in tokenizer(text)]


def build_vocab_from_textfile(
    filepath: str,
    tokenizer: tokenizer.Tokenizer,
    specials: List[str] = ["<unk>", "<pad>", "<bos>", "<eos>"],
) -> Vocab:
    """Build vocab from text corpora."""
    counter = Counter()
    with io.open(filepath, encoding="utf-8") as f:
        for sentence in f:
            counter.update(tokenize(text=sentence, tokenizer=tokenizer))
    return Vocab(counter=counter, specials=specials)


def convert_str_to_idx(
    text: str, tokenizer: tokenizer.Tokenizer, vocab: Vocab
) -> List[int]:
    """
    Convert text into list of integer coded words in text.

    Vocab is used to assign integer index for every word in text.
    """
    return [vocab[token.text] for token in tokenizer(text)]


def extract_archive(from_path, to_path=None, overwrite=False):
    """Extract archive.

    Arguments:
        from_path: the path of the archive.
        to_path: the root path of the extracted files (directory of from_path)
        overwrite: overwrite existing files (False)

    Returns:
        List of paths to extracted files even if not overwritten.

    Examples:
        >>> url = 'http://www.quest.dcs.shef.ac.uk/wmt16_files_mmt/validation.tar.gz'
        >>> from_path = './validation.tar.gz'
        >>> to_path = './'
        >>> torchtext.utils.download_from_url(url, from_path)
        >>> torchtext.utils.extract_archive(from_path, to_path)
        >>> ['.data/val.de', '.data/val.en']
        >>> torchtext.utils.download_from_url(url, from_path)
        >>> torchtext.utils.extract_archive(from_path, to_path)
        >>> ['.data/val.de', '.data/val.en']

    """

    if to_path is None:
        to_path = os.path.dirname(from_path)

    if from_path.endswith((".tar.gz", ".tgz")):
        logging.info("Opening tar file {}.".format(from_path))
        with tarfile.open(from_path, "r") as tar:
            files = []
            for file_ in tar:
                file_path = os.path.join(to_path, file_.name)
                if file_.isfile():
                    files.append(file_path)
                    if os.path.exists(file_path):
                        logging.info("{} already extracted.".format(file_path))
                        if not overwrite:
                            continue
                tar.extract(file_, to_path)
            return files

    elif from_path.endswith(".zip"):
        assert zipfile.is_zipfile(from_path), from_path
        logging.info("Opening zip file {}.".format(from_path))
        with zipfile.ZipFile(from_path, "r") as zfile:
            files = []
            for file_ in zfile.namelist():
                file_path = os.path.join(to_path, file_)
                files.append(file_path)
                if os.path.exists(file_path):
                    logging.info("{} already extracted.".format(file_path))
                    if not overwrite:
                        continue
                zfile.extract(file_, to_path)
        files = [f for f in files if os.path.isfile(f)]
        return files
