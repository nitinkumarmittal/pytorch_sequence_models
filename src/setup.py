"""Setup script."""
from setuptools import find_packages, setup

setup(
    name="pytorch_sequence_models",
    version="0.1",
    packages=find_packages(),
)
