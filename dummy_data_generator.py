from typing import List

import torch
from torch.nn.utils.rnn import pad_sequence


def generate_data(
    batch_size: int,
    max_seq_length: int,
    from_token_idx: int,
    to_token_idx: int,
) -> List[torch.Tensor]:
    """
    Generate arbitrary sequence data.

    Sequences are generated from `from_token_idx` to `to_token_idx`.
    """
    data = []
    for _ in range(batch_size):
        seq_length = max(1, torch.randint(max_seq_length, size=(1,))[0])
        data.append(
            torch.randint(
                low=from_token_idx,
                high=to_token_idx,
                size=(seq_length,),
                dtype=torch.long,
            )
        )
    return data


def process_data(
    data: List[torch.Tensor],
    start_token_idx: int,
    end_token_idx: int,
    pad_token_idx: int,
) -> torch.Tensor:
    """
    Convert batch of non-constant length sequences into
    batch of constant length sequences.

    All sequences are padded with pad token index
    to match longest sequence in batch
    after adding start and end token indexes.
    """
    data = [
        torch.cat(
            [torch.tensor([start_token_idx]), x, torch.tensor([end_token_idx])]
        )
        for x in data
    ]
    return pad_sequence(data, batch_first=True, padding_value=pad_token_idx)
