#  Installation

## Pytorch installation
[pytorch](https://pytorch.org/)

## Pytorch torchtext installation
conda install -c pytorch torchtext

## Install spacy requirements
python -m spacy download en

## To enable tqdm.autonotebook progress bar
```bash
conda install -c conda-forge ipywidgets
jupyter nbextension enable --py widgetsnbextension --sys-prefix
```