import io
import os
from typing import Dict, List, Tuple, Union

import torch
from IPython.display import clear_output
from spacy import load, tokenizer
from torch import nn
from torchtext.utils import download_from_url, extract_archive
from torchtext.vocab import Vocab
from tqdm import tqdm

from pytorch_sequence_models import text_utils

data_path = os.path.join(os.getcwd(), "data")

base_url = (
    "https://raw.githubusercontent.com/multi30k/dataset/master/data/task1/raw/"
)

dataset_urls = {
    "train": {"eng": "train.en.gz", "ger": "train.de.gz"},
    "valid": {"eng": "val.en.gz", "ger": "val.de.gz"},
    "test": {"eng": "test_2016_flickr.en.gz", "ger": "test_2016_flickr.de.gz"},
}


UNK_TOKEN = "<unk>"  # unknown word token
PAD_TOKEN = "<pad>"  # pad token
BOS_TOKEN = "<bos>"  # beginning of sentence token
EOS_TOKEN = "<eos>"  # end of sentence token
specials = [UNK_TOKEN, PAD_TOKEN, BOS_TOKEN, EOS_TOKEN]


def download_extract_data(mode: str) -> Dict[str, str]:
    """
    Download and extract language data.

    Parameters
    ----------
    mode: str
        can be picked from {"train", "valid", "test"}
    """
    filepaths = {}
    for lang, filename in dataset_urls[mode].items():
        filepaths[lang] = extract_archive(
            download_from_url(base_url + filename, root=data_path)
        )[0]
    return filepaths


def prepare_dataset(
    mode: str, eng_vocab: Vocab = None, ger_vocab: Vocab = None
) -> Union[
    List[Tuple[List[int], List[int]]],
    Tuple[List[Tuple[List[int], List[int]]], Vocab, Vocab],
]:
    """
    Prepare data(eng_text, ger_text) for seq2seq model.

    Parameters
    ----------
    mode: str

    eng_vocab: english vocabulary generated from train dataset
        default=None, required to be passed explicitly if `mode!=train`.

    ger_vocab: german vocabulary generated from train dataset
        default=None, required to be passed explicitly if `mode!=train`.

    Returns
    -------
    (dataset, english vocab, german vocab) if mode is "train"
        else dataset.
    """
    print(f"Downloading {mode} data...")
    filepaths = download_extract_data(mode)

    # english nlp
    eng_nlp = load("en_core_web_sm")
    # german nlp
    ger_nlp = load("de_core_news_sm")

    if mode == "train":
        # preparing english and german vocab from train dataset
        clear_output(wait=True)
        print(f"Building {mode} vocab...")
        eng_vocab = text_utils.build_vocab_from_textfile(
            filepath=filepaths["eng"],
            tokenizer=eng_nlp.tokenizer,
            specials=specials,
        )

        ger_vocab = text_utils.build_vocab_from_textfile(
            filepath=filepaths["ger"],
            tokenizer=ger_nlp.tokenizer,
            specials=specials,
        )

    clear_output(wait=True)
    print(f"Tokenizing {mode} dataset...")
    eng_iter = iter(io.open(filepaths["eng"], encoding="utf8"))
    ger_iter = iter(io.open(filepaths["ger"], encoding="utf8"))
    dataset = []

    for (eng_text, ger_text) in tqdm(zip(eng_iter, ger_iter)):
        eng_text_coded = text_utils.convert_str_to_idx(
            text=eng_text, tokenizer=eng_nlp.tokenizer, vocab=eng_vocab
        )
        ger_text_coded = text_utils.convert_str_to_idx(
            text=ger_text, tokenizer=ger_nlp.tokenizer, vocab=ger_vocab
        )
        dataset.append((eng_text_coded, ger_text_coded))

    clear_output(wait=True)
    if mode == "train":
        return (dataset, eng_vocab, ger_vocab)
    return dataset
