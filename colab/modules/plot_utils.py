import itertools
from functools import reduce
from typing import Dict, List, Any

import matplotlib.pyplot as plt
import numpy as np
from IPython.display import clear_output
from matplotlib.ticker import MaxNLocator


def plot_losses(
    losses: Dict[str, List[float]],
    msg=Dict[str, Any]):
    
    clear_output(wait=True)
    fig, ax = plt.subplots(ncols=1, figsize=(4, 4))
    
    for mode, losses in losses.items():
        ax.plot(losses, label=mode)
    
    ax.legend(
        bbox_to_anchor=(1.05, 1), loc='upper left')
    
    title = ""
    for k, v in msg.items():
        title += f"{k}: {v} " 
        
    ax.set_title(title)
    fig.text(s="Epoch", x=".5", y=".01", ha="center")
    fig.text(
        s="Loss", 
        x=".01",
        y=".5", 
        ha="center",
        va="center", 
        rotation="vertical")
    plt.show()
