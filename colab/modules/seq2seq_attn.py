"""Simple attention sequence to sequence model."""
import random
from typing import Tuple

import torch
from torch import nn
from torch.nn import functional as F


class Encoder(nn.Module):
    def __init__(
        self,
        vocab_size: int,
        embedding_dim: int,
        hidden_units: int,
    ):
        super().__init__()

        self.vocab_size = vocab_size
        self.embedding_dim = embedding_dim
        self.hidden_units = hidden_units
        self.embedding_layer = nn.Embedding(
            num_embeddings=vocab_size, embedding_dim=embedding_dim
        )
        self.gru_layer = nn.GRU(
            input_size=embedding_dim,
            hidden_size=hidden_units,
            num_layers=1,
            batch_first=True,
            bias=True,
            dropout=0,
            bidirectional=False,
        )

    def forward(self, x: torch.Tensor) -> Tuple[torch.Tensor, torch.Tensor]:
        """
        Return outputs and hidden states from encoder.

        Parameters
        ----------
        x <- (batch_size, en_max_seq_len_)

        Returns
        -------
        outputs <- (batch_size, en_max_seq_len_, hidden_units)

        hidden_states_t <- (1, batch_size, hidden_units)
        """
        # x <- (batch_size, en_max_seq_len_, en_embedding_dim)
        x = self.embedding_layer(x)

        # outputs <- (batch_size, en_max_seq_len_, hidden_units)
        # hidden_states_t <- (1, batch_size, hidden_units)
        outputs, hidden_states_t = self.gru_layer(x)

        return outputs, hidden_states_t


class Attention(nn.Module):
    def __init__(self, hidden_units: int):
        super().__init__()

        self.hidden_units = hidden_units
        self.fc_1 = nn.Linear(
            in_features=hidden_units, out_features=hidden_units
        )
        self.tanh = nn.Tanh()

    def forward(
        self, de_hidden_states_t: torch.Tensor, en_outputs: torch.Tensor
    ) -> torch.Tensor:
        """
        Return attention weights.

        Parameters
        ----------
        de_hidden_states_t <- (batch_size, hidden_units)

        en_outputs <- (batch_size, en_max_seq_len_, hidden_units)

        Return
        ------
        weights_t <- (batch_size, hidden_units, 1)
        """
        # weights_t <- (batch_size, hidden_units, 1)
        weights_t = self.fc_1(de_hidden_states_t).unsqueeze(-1)
        
        # weights_t <- (batch_size, en_max_seq_len_, 1)
        weights_t = torch.bmm(en_outputs, weights_t)

        # weights_t <- (batch_size, en_max_seq_len_, 1)
        weights_t = nn.functional.softmax(weights_t, dim=1)

        return weights_t


class Decoder(nn.Module):
    def __init__(
        self,
        vocab_size: int,
        embedding_dim: int,
        hidden_units: int,
        attention: nn.Module,
    ):
        super().__init__()

        self.vocab_size = vocab_size
        self.embedding_dim = embedding_dim
        self.hidden_units = hidden_units
        self.embedding_layer = nn.Embedding(
            num_embeddings=vocab_size, embedding_dim=embedding_dim
        )
        self.gru_cell = nn.GRUCell(
            input_size=embedding_dim, hidden_size=hidden_units, bias=True
        )
        self.attention_layer = attention
        self.fc1 = nn.Linear(
            in_features=hidden_units * 2, out_features=vocab_size
        )
    

    def _compute_context_vector_t(
        self, attn_weights_t: torch.Tensor, en_outputs: torch.Tensor
    ) -> torch.Tensor:
        """
        Compute context vector at time-point t.

        Parameters
        ----------
        attn_weights_t <- (batch_size, en_max_seq_len_, 1)

        en_outputs <- (batch_size, en_max_seq_len_, hidden_units)

        Returns
        -------
        context_vector_t <- (batch_size, hidden_units)
        """
        # context_vector_t <- (batch_size, en_max_seq_len_, hidden_units)
        context_vector_t = attn_weights_t * en_outputs

        # context_vector_t <- (batch_size, hidden_units)
        return context_vector_t.sum(dim=1)

    def forward(
        self,
        x: torch.Tensor,
        hidden_states_t: torch.Tensor,
        en_outputs: torch.Tensor,
    ) -> Tuple[torch.Tensor, torch.Tensor]:
        """
        Return hidden states from decoder.

        Parameters
        ----------
        x <- (batch_size, )

        hidden_states_t <- (batch_size, hidden_units)

        en_outputs <- (batch_size, en_max_seq_len_, hidden_units)

        Returns
        -------
        outputs <- (batch_size, de_vocab_size)

        hidden_states_t <- (batch_size, hidden_units)
        """
        # x <- (batch_size, de_embedding_dim)
        x = self.embedding_layer(x)

        # hidden_states_t <- (batch_size, hidden_units)
        hidden_states_t = self.gru_cell(x, hidden_states_t)

        # attention_weights <- (batch_size, en_max_seq_len_, 1)
        attn_weights_t = self.attention_layer(
            de_hidden_states_t=hidden_states_t,
            en_outputs=en_outputs,
        )

        # context_vector_t <- (batch_size, hidden_units)
        context_vector_t = self._compute_context_vector_t(
            attn_weights_t=attn_weights_t, en_outputs=en_outputs
        )

        # outputs <- (batch_size, de_vocab_size)
        outputs = self.fc1(
            torch.cat([hidden_states_t, context_vector_t], dim=1)
        )
        
        return outputs, hidden_states_t


class Seq2Seq(nn.Module):
    def __init__(
        self, encoder: nn.Module, decoder: nn.Module, device: torch.device
    ):
        super().__init__()
        self.encoder = encoder
        self.decoder = decoder
        self.device = device

    def forward(
        self,
        x: torch.Tensor,
        y: torch.Tensor,
        teacher_force_ratio: float = 0.5,
    ) -> torch.Tensor:
        """
        Return output from Seq2Seq model.

        Parameters
        x <- (batch_size, en_max_seq_len_)

        y <- (batch_size, de_max_seq_len_)
        """
        batch_size, de_max_seq_len_ = x.shape[0], y.shape[1]
        de_vocab_size = self.decoder.vocab_size

        # de_outputs <- (batch_size, de_max_seq_len_, de_vocab_size)
        de_outputs = torch.zeros(
            size=(batch_size, de_max_seq_len_, de_vocab_size)
        ).to(self.device)

        # en_outputs <- (batch_size, en_max_seq_len_, hidden_units)
        # en_hidden_states_t <- (batch_size, hidden_units)
        en_outputs, en_hidden_states_t = self.encoder(x)

        # Every sequence in x and y are assumed to start with
        # <START_IDX> (start of sentence) token index

        # de_x_t <- (batch_size, ), <START_IDX>
        de_x_t = y[:, 0]

        # de_hidden_states_t <- (batch_size, hidden_units)
        de_hidden_states_t = en_hidden_states_t[0]

        for t in range(1, de_max_seq_len_):

            # de_outputs_t <- (batch_size, de_vocab_size)
            # de_hidden_states_t <- (batch_size, hidden_units)
            de_outputs_t, de_hidden_states_t = self.decoder(
                x=de_x_t,
                hidden_states_t=de_hidden_states_t,
                en_outputs=en_outputs,
            )

            # de_outputs <- (batch_size, de_max_seq_len_, de_vocab_size)
            de_outputs[:, t, :] = de_outputs_t

            # de_x_t <- (batch_size, )
            de_best_x_t = torch.argmax(
                F.log_softmax(de_outputs_t, dim=1), dim=1
            )

            # to improve training, use teacher_forcing
            # de_x_t <- (batch_size, )
            de_x_t = (
                y[:, t]
                if random.random() > teacher_force_ratio
                else de_best_x_t
            )

        # de_outputs <- (batch_size, de_max_seq_len_, de_vocab_size)
        return de_outputs
