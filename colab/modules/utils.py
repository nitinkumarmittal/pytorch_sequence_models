from pickle import dump, load
import os
from typing import Any

def prepare_path(filename: str, path: str=None)->str:
  format = ".pickle"
  if len(filename) <= len(format) or filename[len(format)] != format:
    filename+=format
  if path is not None:
    filename=os.path.join(path, filename)
  return filename

def dump_pickle(obj: Any, filename: str, path:str=None):
  filename=prepare_path(filename, path)
  with open(file=filename, mode='wb') as f:
    dump(obj, f)

def load_pickle(filename: str, path: str=None)->Any:
  filename=prepare_path(filename, path)
  with open(file=filename, mode='rb') as f:
    return load(f)