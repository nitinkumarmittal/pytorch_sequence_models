import io
from collections import Counter
from typing import List

import torch
from spacy import tokenizer
from torchtext.vocab import Vocab


def tokenize(text: str, tokenizer: tokenizer.Tokenizer) -> List[str]:
    """Return list of str tokens."""
    return [token.text for token in tokenizer(text)]


def build_vocab_from_textfile(
    filepath: str,
    tokenizer: tokenizer.Tokenizer,
    specials: List[str] = ["<unk>", "<pad>", "<bos>", "<eos>"],
    lower:bool=True,
    num_lines: int=None,
) -> Vocab:
    """Build vocab from text corpora."""
    counter = Counter()
    
    with io.open(filepath, encoding="utf-8") as f:
        for i, sentence in enumerate(f):
            if lower:
                sentence=sentence.lower()
            counter.update(tokenize(text=sentence, tokenizer=tokenizer))
            if num_lines is not None and (i+1) > num_lines:
                break
    return Vocab(counter=counter, specials=specials)


def convert_str_to_idx(
    text: str, tokenizer: tokenizer.Tokenizer, vocab: Vocab
) -> List[int]:
    """
    Convert text into list of integer coded words in text.

    Vocab is used to assign integer index for every word in text.
    """
    return [vocab[token.text] for token in tokenizer(text)]
